Hi!

   Here is a my solution of Your task.
 
 First of all i want to notice few thing:
 
 1)Solution contain two .php file so i must be launched on apache server or something like this(i use xampp).
 
 2)Recommended is full hd display
 
   
   Technology used in solution: 
 
 1)HTML 5, 
 
 2)CSS 3, 
 
 3)JavaScrip, 
 
 4)PHP(prepared script to get JSON data from file, 
 
 5)JSON data, 
 
 6)Google Charts: LineChar and ColumnChart. I choose it, because they have rich and detailed documentation to learn how it work and describe implementation methods.
   I know that there are others for example: Chart.js but as you can see, I choose Google Charts. Charts are based on JQuery.
 
 7)I Also used Google Web Font and Fontello for better looking template.
 
 Solution working on all popular web browsers(Chrome, Opera, Firefox). Important syntax for chrome "background:-webkit" in css style stylesheet.
 
 You wrote: "It would be interested to see how much you can achieve with minimal use of graphic (image) files.", so there is NO IMAGE FILES, everything is done by CSS,
 only "down-arrow" symbol is from "Fontello, but its not image file, its just font.
 
 Hope You like my solution, I'm waiting for hearing from you.
 
 
 Best regards!
 
 Piotr Kardas
 
 531 169 015
 
 piotr1689@o2.pl
 
 
 
 