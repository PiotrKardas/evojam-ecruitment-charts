			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart1);
			
		  function drawChart1() {
			var jsonData = $.ajax({
			  url: "getData1.php",
			  dataType: "json",
			  async: false
			  }).responseText;
			  
		  var data = new google.visualization.DataTable(jsonData);
		  
		  var options = {
			  title: 'Revenue per week',
			  vAxis: {format: ''},
			  colors: ['#ED6E37', '#259E01', '#15A0C8'],
			  height: 290,
			  width: 550,
			  legend: { position: 'bottom' },
			  pointSize: 10,
			  titleTextStyle: 
			  {
				 color: '#04375C',
				 fontSize: 15 
			  },
			 lineWidth: 5,
			 animation: 
			 { 
				startup: true,
				duration: 600,
				easing: 'out'
			 },
			 fontName: 'Open Sans',
			 chartArea:{left:45,width:'100%'},
			 
			};
			var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}