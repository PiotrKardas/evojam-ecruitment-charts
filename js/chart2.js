			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart2);
		  
		  function drawChart2() {
			var jsonData = $.ajax({
			  url: "getData2.php",
			  dataType: "json",
			  async: false
			  }).responseText;
		  
		  var data = new google.visualization.DataTable(jsonData);
			
		  var options = {
			  title: 'Installations per day',
			  bars: 'vertical',
			  vAxis: {format: ''},
			  colors: ['#ED6E37', '#259E01', '#15A0C8'],
			  height: 290,
			  width: 550,
			  bar: {groupWidth: '40%'},
			  legend: { position: 'bottom' },
			  titleTextStyle: 
			  {
				 color: '#04375C',
				 fontSize: 15 
			  },
			 animation: 
			 { 
				startup: true,
				duration: 600,
				easing: 'out'
			 },
			 fontName: 'Open Sans',
			 chartArea:{left:45,width:'100%' },
			 
			};
			var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
			chart.draw(data, options);
		}